// Add to ID token - roles
async function add_roles_id_token (user, context, callback) {
  let roles = (context.authorization || {}).roles;
  const claim = `${configuration.claim_namespace}/roles`;

  // When non-default user is linked in link_non_default, context.authorization
  // does not reflect the default user. Fetch user's roles in this case. This
  // only happens directly after linking non-default user
  if (!(roles && roles.length)) {
    if (!global.$auth0_client) global.$create_auth0_client();

    const roles_ = await global.$auth0_client.getUserRoles({
      id: user.user_id
    });

    roles = roles_.map(r => r.name);
  }

  context.idToken = {
    ...context.idToken,
    [claim]: roles
  };

  callback(null, user, context);
}
