// Add to access token -  company uuid
function add_company_uuid_access_token (user, context, callback) {
  const claim = `${configuration.claim_namespace}/company_uuid`;

  if (user.app_metadata) {
    context.accessToken = {
      ...context.accessToken,
      [claim]: user.app_metadata.company_uuid
    };
  }

  callback(null, user, context);
}
