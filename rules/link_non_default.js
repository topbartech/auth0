// Link non-default users
async function link_non_default (user, context, callback) {
  if (user.app_metadata && user.app_metadata.default) {
    return callback(null, user, context);
  }

  if (!global.$auth0_client) global.$create_auth0_client();

  const users = await global.$auth0_client.getUsersByEmail(user.email);

  let default_user;
  for (const user of users) {
    if (user.app_metadata && user.app_metadata.default) {
      default_user = user;
      break;
    }
  }

  if (!default_user) return callback(new Error('Your account does not exist.'));
  console.log('default user found:', default_user.user_id);

  console.log('linking:', user.user_id);
  const [provider, user_id] = user.user_id.split('|');
  await global.$auth0_client.linkUsers(default_user.user_id, {
    provider,
    user_id,
    connection_id: context.connectionID
  });

  context.primaryUser = default_user.user_id;
  callback(null, default_user, context);
}
