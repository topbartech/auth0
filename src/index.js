import Vue from 'vue';
import createAuth0Client from '@auth0/auth0-spa-js';

import { translate_claims } from './util';

export * from './util';
export * from './middleware';

export let instance;
export const useAuth0 = ({
  router,
  domain,
  client_id,
  audience,
  redirect_uri,
  claim_namepace,
  cache_location = 'memory'
}) => {
  if (instance) return instance;
  instance = new Vue({
    data () {
      return {
        loading: true,
        authenticated: false,
        user: {},
        client: null,
        popupOpen: false,
        error: null
      };
    },
    async created () {
      this.client = await createAuth0Client({
        domain,
        client_id,
        audience,
        redirect_uri,
        cacheLocation: cache_location
      });

      await this.load();
      if (this.authenticated) this.loading = false;
    },
    methods: {
      async wait_ready () {
        let unwatch;
        await new Promise(resolve => {
          unwatch = this.$watch(
            'client',
            val => val && resolve(),
            { immediate: true }
          );
        });
        unwatch();
      },
      async wait_auth () {
        let unwatch;
        await new Promise(resolve => {
          unwatch = this.$watch(
            'authenticated',
            val => val && resolve(),
            { immediate: true }
          );
        });
        unwatch();
      },
      async load () {
        this.user = await this.client.getUser();
        if (this.user) {
          translate_claims(this.user, claim_namepace, [
            'roles'
          ]);
          this.user.roles = new Set(this.user.roles);
        }

        this.authenticated = !!this.user;
      },
      async loginWithPopup (o) {
        this.popupOpen = true;

        try {
          await this.client.loginWithPopup(o);
        } catch (e) {
          this.error = e;
        }

        await this.load();
        this.popupOpen = false;
      },
      async handleRedirectCallback () {
        this.loading = true;

        let out;
        try {
          out = await this.client.handleRedirectCallback();
        } catch (e) {
          this.error = e;
          console.warn('[auth0]', e);
          // Check if AuthenticationError (which is not exposed)
          if (e.appState) throw e;
        }

        await this.load();
        this.loading = false;
        return out;
      },
      loginWithRedirect (o) {
        return this.client.loginWithRedirect(o);
      },
      getIdTokenClaims (o) {
        return this.client.getIdTokenClaims(o);
      },
      getTokenSilently (o) {
        return this.client.getTokenSilently(o);
      },
      getTokenWithPopup (o) {
        return this.client.getTokenWithPopup(o);
      },
      logout (o) {
        return this.client.logout({
          returnTo: redirect_uri,
          ...o
        });
      }
    }
  });

  return instance;
};

export const Auth0 = {
  install (Vue, options) {
    Vue.prototype.$auth = useAuth0(options);
  }
};

export default Auth0;
