// Require verified email
function email_verified(user, context, callback) {
  if (user.email_verified) {
    return callback(null, user, context);
  }

  return callback(new UnauthorizedError('Please verify your email.'));
}