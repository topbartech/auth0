// Init global object
async function init_global (user, context, callback) {
  global.$auth0_client = null;
  global.$create_auth0_client = () => {
    const ManagementClient = require('auth0@2.23.0').ManagementClient;
    global.$auth0_client = new ManagementClient({
      domain: auth0.domain,
      clientId: configuration.client_id,
      clientSecret: configuration.client_secret,
    }); 

    return global.$auth0_client;
  };

  return callback(null, user, context);
}
