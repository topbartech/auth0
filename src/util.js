import jwksRsa from 'jwks-rsa';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';

export class Auth0Error extends Error {
  constructor (message, error) {
    super(message);
    this.name = 'Auth0Error';
    this.error = error;
  }
}
export class TokenError extends Auth0Error {
  constructor (message, error) {
    super(message, error);
    this.name = 'TokenError';
  }
}
export class PermissionError extends Auth0Error {
  constructor (scope) {
    super(`Missing required scope (${scope})`);
    this.name = 'PermissionError';
    this.scope = scope;
  }
}

export function translate_claims (obj, namespace, claims) {
  for (const claim_name of claims) {
    const claim = `${namespace}/${claim_name}`;
    if (obj[claim]) {
      obj[claim_name] = obj[claim];
    }
  }
}

export function translate_auth0_uuid (id) {
  return id.split('|')[1];
}
export function translate_uuid_auth0 (uuid) {
  return `auth0|${uuid}`;
}

export function get_pub_key (jwks_client, token) {
  return new Promise((resolve, reject) => {
    const { header, payload } = jwt.decode(token, { complete: true });

    jwks_client.getSigningKey(header.kid, (err, key) => {
      if (err) return reject(err);
      return resolve(key.publicKey || key.rsaPublicKey);
    });
  });
}

export async function generate_password () {
  return new Promise((resolve, reject) => {
    return crypto.randomBytes(60, (err, res) => {
      if (err) return reject(err);
      return resolve(res.toString('base64'));
    });
  });
}
