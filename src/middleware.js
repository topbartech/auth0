import jwt from 'jsonwebtoken';
import jwks_rsa from 'jwks-rsa';
import {
  Auth0Error,
  TokenError,
  PermissionError,
  get_pub_key,
  translate_claims,
  translate_auth0_uuid
} from './util';

export function useAuth0Middleware (options = {}) {
  const {
    audience,
    issuer,
    algorithm = ['RS256'],
    claim_namespace,
    claims = [],
  } = options;

  const jwks_client = jwks_rsa({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `${issuer}.well-known/jwks.json`
  });

  return async function authenticated_security_handler (req, scopes, definition) {
    if (req.headers.authorization) {
      try {
        const [, token] = req.headers.authorization.split(' ');

        req.user = jwt.verify(token, await get_pub_key(jwks_client, token), {
          audience,
          issuer,
          algorithm,
        });
      } catch (err) {
        throw new TokenError('Token invalid or expired.', err);
      }

      const permissions = req.user.permissions = new Set(req.user.permissions);
      req.user.user_uuid = translate_auth0_uuid(req.user.sub);

      if (claim_namespace) {
        translate_claims(req.user, claim_namespace, claims);
      }

      // Check scope
      if (!scopes.length) return true;
      for (const scope of scopes) {
        if (!permissions.has(scope)) throw new PermissionError(scope);
      }

      return true;
    }

    throw new TokenError('Missing authorization header');
  }
}
